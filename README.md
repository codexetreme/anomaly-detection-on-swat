# Anomaly Detection On the SWaT Dataset


## To get the dataset goto: 
[Dataset](https://drive.google.com/open?id=1eTFSicMIUdE4frwvpYdzbpPYlRxHa7ds)

and place the dataset folder in the root folder of this repo


## Build the docker 

`docker build -t codexetreme/dbmateswat -f fab/d/base.Dockerfile fab/d`

## Run locally

#### 1. Run the dockers
`./sd`
this will start the dockers for the SQL server and client 

### 2. Seed the db
inside the docker do,
`./fab/sh/local_up.sh`
This will load the dataset into PostgreSQL

#### 3. Stop the dockers
`./std`
this will stop all the dockers created in step 1

---
> NOTE: This needs [docker (install steps)](https://docs.docker.com/install/) and [docker-compose (install steps)](https://docs.docker.com/compose/install/)


## Ports (localhost:PORT)
|Program | PORT|
|:--------:|-----|
|PgAdmin |3605|
PostgresDBServer | 5400|

## Processing From the RAW dataset 

>(NOTE: you DON'T have to do this, if you just download the dataset folder from the above link)

- unzip the folder `Physical-20180110T072930Z-001.zip`
- save the `.xlsx` files as `.csv`'s and set their delimiter to `;`
- Then open the `.csv` files and delete the topmost row ( this is the one that reads P1, P2, etc)
- Rename the last column (Normal/Attack) to `type`
- save these files to the path, `/dataset/swat/Physical/[name].csv` (NOTE: please leave the csv files names same as the original names of the xlsx files) 