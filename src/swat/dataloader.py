import numpy as np
import torch
from torch.utils.data import Dataset, DataLoader
import psycopg2
from psycopg2.sql import Identifier, SQL
from sklearn.preprocessing import StandardScaler

DEC2FLOAT = psycopg2.extensions.new_type(
    psycopg2.extensions.DECIMAL.values,
    'DEC2FLOAT',
    lambda value, curs: float(value) if value is not None else None)
psycopg2.extensions.register_type(DEC2FLOAT)


class SWaTDataset(Dataset):

    def __init__(self, config, type):
        self.config = config
        self.type = type
        # creates db connection
        self.connection, self.cursor = self.make_conn()
        self.tableIdentifiers = [Identifier(self.config.db.schema_name), Identifier(self.config.db.table_name)]
        self.headers_to_idx, self.idx_to_headers = self.make_headers_to_idx()
        self.len = self.get_len()

    def get_len(self):
        self.cursor.execute(SQL("SELECT count(*) FROM {}.{}").format(*self.tableIdentifiers))
        record = self.cursor.fetchone()
        return record[0]  # (size,)
        pass

    def make_conn(self):
        try:
            connection = psycopg2.connect(user=self.config.db.user,
                                          password=self.config.db.password,
                                          host=self.config.db.host,
                                          port=self.config.db.port,
                                          database=self.config.db.database)

            cursor = connection.cursor()
            # Print PostgreSQL Connection properties
            print(connection.get_dsn_parameters(), "\n")

            # Print PostgreSQL version
            cursor.execute("SELECT version();")
            record = cursor.fetchone()
            print("You are connected to - ", record, "\n")
            return connection, cursor
        except (Exception, psycopg2.Error) as error:
            print("Error while connecting to PostgreSQL", error)
            return None, None

    def conn_close(self):
        if self.connection:
            self.cursor.close()
            self.connection.close()
            print("PostgreSQL connection is closed")

    def __len__(self):
        return self.len

    def __getitem__(self, idx):
        return idx

    def make_headers_to_idx(self):
        self.cursor.execute(
            'select column_name, ordinal_position from information_schema.columns where table_schema= %s and table_name=%s',
            (self.config.db.schema_name, self.config.db.table_name))
        record = self.cursor.fetchall()
        idx_to_headers = {record[i][1] - 1: record[i][0] for i in range(0, len(record))}
        headers_to_idx = {record[i][0]: record[i][1] - 1 for i in range(0, len(record))}
        return headers_to_idx, idx_to_headers

    def get_columns(self, column_list, inclusive=True):
        if inclusive:
            return column_list
        else:
            cols = []
            for h in self.headers_to_idx.keys():
                if h not in column_list:
                    cols.append(Identifier(h))
            return tuple(cols)

    def collate_fn(self, batch):

        # to make sure time series is continous, we need to lookback upto lookbakk-1
        # additional elments as well
        lookback = 5
        lowest_id = min(batch)
        batch.extend(range(lowest_id-(lookback-1),lowest_id))

        cols = self.get_columns(['timestamp', 'type'], inclusive=False)
        # http://aklaver.org/wordpress/2018/04/21/building-dynamic-sql-using-psycopg2/
        # for multiple columns in a dynamic sql query, this is the format followed, see the above link for more details
        self.cursor.execute(
            SQL("SELECT {} FROM {}.{} WHERE id IN %s").format(SQL(',').join(cols), *self.tableIdentifiers),
            (tuple(batch),))
        records = self.cursor.fetchall()
        values = np.array(records)

        self.cursor.execute(
            SQL("SELECT {} FROM {}.{} WHERE id IN %s").format(Identifier('type'), *self.tableIdentifiers),
            (tuple(batch),))
        labels = self.cursor.fetchall()
        values, labels = make_temporal(values,labels, 5)# [N, timesteps, 1, 52] , [1]
        values = values.squeeze(2) # [N, timesteps, 52]
        scaler = StandardScaler().fit(flatten(values))
        valuesScaled = scale(values, scaler)
        return valuesScaled, labels

def make_temporal(values, labels, lookback):
    x = []
    y = []
    for i in range (values.shape[0]-lookback+1):
        temp = []
        for j in range(0,lookback):
            temp.append(values[[(i+j)], :]) # [1, 52]
        x.append(temp) # [N, lookback, 1, 52]
        y.append(i+lookback-1)

    return np.array(x),y


def flatten(X):
    '''
    Flatten a 3D array.
    
    Input
    X            A 3D array for lstm, where the array is sample x timesteps x features.
    
    Output
    flattened_X  A 2D array, sample x features.
    '''
    flattened_X = np.empty((X.shape[0], X.shape[2]))  # sample x features array.
    for i in range(X.shape[0]):
        flattened_X[i] = X[i, X.shape[1]-1, :]
    return(flattened_X)


def scale(X, scaler):
    '''
    Scale 3D array.

    Inputs
    X            A 3D array for lstm, where the array is sample x timesteps x features.
    scaler       A scaler object, e.g., sklearn.preprocessing.StandardScaler, sklearn.preprocessing.normalize
    
    Output
    X            Scaled 3D array.
    '''
    for i in range(X.shape[0]):
        X[i, :, :] = scaler.transform(X[i, :, :])
        
    return X