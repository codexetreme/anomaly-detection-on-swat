import torch
import torch.nn as nn
import torch.nn.functional as F


class EncoderRNN(nn.Module):
    def __init__(self, input_size, batch_first=True, bidirectional=False, device=None):
        super(EncoderRNN, self).__init__()
        self.hidden_size1 = 32
        self.hidden_size2 = 16
        self.device = device
        self.gru1 = nn.GRU(input_size, self.hidden_size1, batch_first=batch_first, bidirectional=bidirectional)
        self.gru2 = nn.GRU(self.hidden_size1, self.hidden_size2, batch_first=batch_first, bidirectional=bidirectional)

    def forward(self, x, hidden=None):
        if hidden:
            output, hidden = self.gru1(x, hidden)
            output, hidden = self.gru2(output, hidden)
        else:
            output, hidden = self.gru1(x)
            output, hidden = self.gru2(output)
        return output, hidden

    def initHidden(self):
        return torch.zeros(1, 1, self.hidden_size, device=self.device)


class DecoderRNN(nn.Module):
    def __init__(self, input_size, batch_first=True, bidirectional=False, device=None):
        super(DecoderRNN, self).__init__()
        self.hidden_size1 = 16
        self.hidden_size2 = 32
        self.device = device

        self.gru1 = nn.GRU(input_size, self.hidden_size1, batch_first=batch_first, bidirectional=bidirectional)
        self.gru2 = nn.GRU(self.hidden_size1, self.hidden_size2, batch_first=batch_first, bidirectional=bidirectional)
        self.out = nn.Linear(32, 52)
        self.timeDistributed = TimeDistributed(self.out)

    def forward(self, x, hidden=None):

        if hidden:
            output, hidden = self.gru1(x, hidden)
            output, hidden = self.gru2(output, hidden)
        else:
            output, hidden = self.gru1(x)
            output, hidden = self.gru2(output)
        
        output = self.timeDistributed(output)
        return output, hidden

    def initHidden(self):
        return torch.zeros(1, 1, self.hidden_size, device=self.device)


class TimeDistributed(nn.Module):
    def __init__(self, module, batch_first=False):
        super(TimeDistributed, self).__init__()
        self.module = module
        self.batch_first = batch_first

    def forward(self, x):

        if len(x.size()) <= 2:
            return self.module(x)

        # Squash samples and timesteps into a single axis
        x_reshape = x.contiguous().view(-1, x.size(-1))  # (samples * timesteps, input_size)

        y = self.module(x_reshape)

        # We have to reshape Y
        if self.batch_first:
            y = y.contiguous().view(x.size(0), -1, y.size(-1))  # (samples, timesteps, output_size)
        else:
            y = y.view(-1, x.size(1), y.size(-1))  # (timesteps, samples, output_size)

        return y



class AutoEncoderDecoderModel(nn.Module):
     def __init__(self, input_size=52, batch_first=True):
        super(AutoEncoderDecoderModel, self).__init__()
        self.batch_first = batch_first

        self.encoder = EncoderRNN(input_size=52)

        self.decoder = DecoderRNN(input_size=16)
        

     def forward(self,x):
        x,hidden = self.encoder(x)  # [ N, timesteps, 16]
        x = self.decoder(x)  # [ N, timesteps, 52]
        return x