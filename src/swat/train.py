from swat.dataloader import SWaTDataset, flatten
from torch.utils.data import DataLoader
import torch
import numpy as np
from tqdm import tqdm
from swat.model import AutoEncoderDecoderModel
import torch.optim as optim
import torch.nn as nn

def train(config):
    print('Starting Training')

    config.hyperparams.batch_size = int(config.hyperparams.batch_size)
    config.training.current_epoch = int(config.training.current_epoch)
    config.training.total_epochs = int(config.training.total_epochs)

    device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu:0')
    print('selected device: {}'.format(device))
    print('loading dataset...')
    dataset = SWaTDataset(config, type='train')
    loader = DataLoader(dataset, batch_size=config.hyperparams.batch_size, shuffle=False, collate_fn=dataset.collate_fn)
    dataset_val = SWaTDataset(config, type='validation')
    val_loader = DataLoader(dataset_val, batch_size=config.hyperparams.batch_size, shuffle=True)


    model = AutoEncoderDecoderModel().to(device)

    model.double()
    print ('Model Summary: ', model)

    mseLoss = nn.MSELoss()
    optimizer = optim.Adam(model.parameters(), lr=0.0001)

   

    with torch.autograd.detect_anomaly():
        for epoch in tqdm(range(config.training.current_epoch, config.training.total_epochs + 1)):
            for i, (data, labels) in enumerate(loader):

                data = torch.from_numpy(data).squeeze(2)  # [N, timesteps ,52]

                data = data.to(device)

                optimizer.zero_grad()                

                output,hidden = model(data)
                
                loss = mseLoss(output,data)
                loss.backward()
                optimizer.step()
                pass

