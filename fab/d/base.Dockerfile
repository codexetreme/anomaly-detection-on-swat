FROM amacneil/dbmate as dbmate
FROM python:3.7-slim-buster
COPY --from=dbmate /dbmate \
    /usr/local/bin/dbmate

# RUN echo "deb http://apt.postgresql.org/pub/repos/apt/ stretch-pgdg main" >> /etc/apt/sources.list.d/pgdg.list \
#     && wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add - \
#     && apt-get update \
#     && apt-get install -y --no-install-recommends \
#     postgresql-client-10 \
#     && rm -rf /var/lib/apt/lists/*

COPY . /src
WORKDIR /src

RUN useradd -s /bin/bash -d /home/puffy/ -m -G sudo puffy
# RUN passwd puffy

# shortcut for using the most probable user id of the host system: 1000
RUN apt-get update \
    && apt-get install sudo less -y \
    && usermod -aG sudo puffy \
    && echo '%sudo ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers

USER puffy


ENTRYPOINT [ "/src/fab/sh/entryPoint.sh" ]
