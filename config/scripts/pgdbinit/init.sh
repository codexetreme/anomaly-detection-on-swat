#!/bin/bash
set -e

# \list to list dbs
#  \dn to list schemas
#  \dt to list tables


# psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" <<-EOSQL
#  CREATE DATABASE gdb;
#  \connect gdb;
#  CREATE SCHEMA graphqlschema;
#  GRANT ALL PRIVILEGES ON DATABASE gdb TO "$POSTGRES_USER";
# EOSQL

# Can create db during postgres init too from here

# But prefer using similar methods for dev and prod, i.e. through dbmate instead of there

#psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" <<-EOSQL
#  CREATE DATABASE teaxr;
#  CREATE DATABASE teaxrtest;
#  CREATE DATABASE metrics;
#  CREATE DATABASE metricstest;
#EOSQL

#### Postgres psql commands

#Connect to command prompt
# psql -h dev-postgresql.dev.svc.cluster.local -p 5432 -U postgres -W

#  \l - Display database
#  \c - Connect to database
#  \dn - List schemas
#  \dt - List tables inside public schemas
#  \dt schema1. - List tables inside particular schemas. For eg: 'schema1'.
# (Notice the .[dot] after schema name)
#  \d test_schema.test_table - Show the test_schema.test_table definition
#  SELECT * FROM teaxr.role; - Run a SQL statement
# (Notice the ;[semicolon] at the end of the SQL statement

#To create a new db, user and pass:
#CREATE DATABASE yourdbname;
#CREATE USER youruser WITH ENCRYPTED PASSWORD 'yourpass';
#GRANT ALL PRIVILEGES ON DATABASE yourdbname TO youruser;


#ALTER USER the_username WITH PASSWORD 'the new password';

## For RDS

#GRANT ALL PRIVILEGES ON DATABASE teaxr TO teaxruser;
#GRANT ALL PRIVILEGES ON SCHEMA teaxr TO teaxruser;
#GRANT ALL ON ALL TABLES IN SCHEMA teaxr TO teaxruser;
#GRANT ALL ON ALL SEQUENCES IN SCHEMA teaxr TO teaxruser;
#GRANT SELECT ON information_schema.table_constraints, information_schema.key_column_usage, information_schema.columns, information_schema.views, information_schema.schemata, information_schema.routines TO teaxruser;
#GRANT SELECT ON pg_catalog.pg_constraint, pg_catalog.pg_class, pg_catalog.pg_namespace, pg_catalog.pg_attribute, pg_catalog.pg_proc, pg_catalog.pg_available_extensions, pg_catalog.pg_statio_all_tables, pg_catalog.pg_description TO teaxruser;

#GIVE PERMISSIONS FOR ALL VIEWS
#SELECT 'GRANT SELECT ON ' || quote_ident(schemaname) || '.' || quote_ident(viewname) || ' TO teaxruser;' FROM pg_views WHERE schemaname = 'teaxr';
# EXECUTE THE OUTPUT OF ABOVE QUERY TO GRANT ALL PERMISSIONS ON VIEWS

#GRANT ALL PRIVILEGES ON DATABASE metrics TO metricsuser
#GRANT ALL PRIVILEGES ON SCHEMA metrics TO metricsuser;
#GRANT ALL ON ALL TABLES IN SCHEMA metrics TO metricsuser;
#GRANT ALL ON ALL SEQUENCES IN SCHEMA metrics TO metricsuser;
#GRANT SELECT ON information_schema.table_constraints, information_schema.key_column_usage, information_schema.columns, information_schema.views, information_schema.schemata, information_schema.routines TO metricsuser;
#GRANT SELECT ON pg_catalog.pg_constraint, pg_catalog.pg_class, pg_catalog.pg_namespace, pg_catalog.pg_attribute, pg_catalog.pg_proc, pg_catalog.pg_available_extensions, pg_catalog.pg_statio_all_tables, pg_catalog.pg_description TO metricsuser;

#GIVE PERMISSIONS FOR ALL VIEWS
#SELECT 'GRANT SELECT ON ' || quote_ident(schemaname) || '.' || quote_ident(viewname) || ' TO metricsuser;' FROM pg_views WHERE schemaname = 'metrics';
# EXECUTE THE OUTPUT OF ABOVE QUERY TO GRANT ALL PERMISSIONS ON VIEWS
