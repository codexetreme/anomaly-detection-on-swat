from config import Configuration
from swat.train import train


def main():
    configuration = Configuration('../project.ini')
    config = configuration.get_config_options()
    if config.globals.is_train:
        train(config)


if __name__ == '__main__':
    main()
