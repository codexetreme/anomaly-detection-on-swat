#!/bin/bash
#https://unix.stackexchange.com/a/1498 Aliases don't working in non-interactive shell scripts.
shopt -s expand_aliases
source ./.bashinit

#set -x
echo "DB: swat"
dbmateswat -d db/swat/seed down; for n in {1..8}; do dbmateswat down; done;
dbmateswat up; dbmateswat -d db/swat/seed up;

echo "DB: wadi"
dbmatewadi -d db/wadi/seed down; for n in {1..8}; do dbmatewadi down; done;
dbmatewadi up; dbmatewadi -d db/wadi/seed up;